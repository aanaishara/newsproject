from django.urls import path
from .views import *


app_name="newsapp"
urlpatterns=[
path("",HomeView.as_view(), name="home"),
path("category/<int:pk>/detail",CategoryDetailView.as_view(),name="categorydetail"),
path("news/<int:pk>/newsdetail/",NewsDetailView.as_view(), name="newsdetail"),
path("contact/create/",ContactCreateView.as_view(), name="contactcreate"),


path("news-admin/category/create/",CategoryCreateView.as_view(), name="categorycreate"),

path("news-admin/news/create/",NewsCreateView.as_view(), name="newscreate"),

path("subscriber/",SubscriberCreateView.as_view(),name="subscriber"),





]