from django.conf import settings
from django.core.mail import send_mail

from django.shortcuts import render
from django.views.generic import *
from .forms import *

from .models import *
import requests


class BaseMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["allcats"] = Category.objects.all()
        context["subsform"] = SubscriberForm
        return context


class HomeView(BaseMixin, TemplateView):
    template_name = "home.html"


class CategoryDetailView(BaseMixin, DetailView):
    template_name = "categorydetail.html"
    model = Category
    context_object_name = "categoryobject"


class NewsDetailView(BaseMixin, DetailView):
    template_name = "newsdetail.html"
    model = News
    context_object_name = "newsobject"


class CategoryCreateView(BaseMixin, CreateView):
    template_name = "categorycreate.html"
    form_class = CategoryForm
    success_url = "/"


class NewsCreateView(BaseMixin, CreateView):
    template_name = "newscreate.html"
    form_class = NewsForm
    success_url = "/"

    def form_valid(self, form):
        a = form.cleaned_data["title"]
        b = form.cleaned_data["category"]
        print(a, b)

        return super().form_valid(form)


class ContactCreateView(BaseMixin, CreateView):
    template_name = "contactcreate.html"
    form_class = ContactForm
    success_url = "/"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        response = requests.get(
            "https://ratopati.com/jsonapi/get_recent_posts/")
        results = eval(response.text)
        results = results['posts']
        context['allarticles'] = results
        return context

    def form_valid(self, form):
        form_name = form.cleaned_data["name"]
        form_email = form.cleaned_data["email"]

        subject = 'Site contact form'
        from_email = settings.EMAIL_HOST_USER
        contact_message = "Thanks for contacting us."
        send_mail(subject,
                  contact_message,
                  from_email,
                  [form_email],
                  fail_silently=False)
        return super().form_valid(form)


class SubscriberCreateView(BaseMixin, CreateView):
    template_name = "subscriber.html"
    form_class = SubscriberForm
    success_url = "/"

    def form_valid(self, form):

        form_email = form.cleaned_data["email"]

        subject = 'Site contact form'
        from_email = settings.EMAIL_HOST_USER
        contact_message = "Thanks for subscribing us."
        send_mail(subject,
                  contact_message,
                  from_email,
                  [form_email],
                  fail_silently=False)
        return super().form_valid(form)
    # def get_context_data(self,**kwargs):
    # 	context=super().get_context_data(**kwargs)
    # 	context["subsform"]=SubscriberForm
    # 	return context


# Create your views here.
