from django import forms
from .models import *


class CategoryForm(forms.ModelForm):
	class Meta:
		model=Category
		fields=["title","image"]


class NewsForm(forms.ModelForm):
	class Meta:
		model=News
		fields=["title","category","image","content","author"]


class CommentForm(forms.ModelForm):
	class Meta:
		model=Comment
		fields=["name","news","text"]


class ContactForm(forms.ModelForm):
	class Meta:
		model=Contact
		fields=["name","address","contact","email"]

class SubscriberForm(forms.ModelForm):
	class Meta:
		model=Subscriber
		fields=["email"]


