from django.contrib import admin
from .models import *

admin.site.register([Category,News,Comment,Contact,Subscriber])

# Register your models here.
