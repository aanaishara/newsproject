from django.db import models

# Create your models here.
class Category(models.Model):
	title=models.CharField(max_length=200)
	image=models.ImageField(upload_to="category")
	
	def __str__(self):
		return self.title

		#manayto one relation in category and news
class News(models.Model):
	title=models.CharField(max_length=300)
	category=models.ForeignKey(Category,on_delete=models.CASCADE)
	image=models.ImageField(upload_to="news")
	content=models.TextField()
	author=models.CharField(max_length=100,null=True,blank=True)
	date=models.DateTimeField(auto_now_add=True)
	view_count=models.PositiveIntegerField(default=0)
	
	def __str__(self):
		return self.title

class Comment(models.Model):
	name=models.CharField(max_length=200)
	news=models.ForeignKey(News,on_delete=models.CASCADE)
	text=models.TextField()
	date=models.DateTimeField(auto_now_add=True)
	
	def __str__(self):
		return self.name

class Contact(models.Model):
	name=models.CharField(max_length=200)
	address=models.CharField(max_length=200)
	contact=models.CharField(max_length=20)
	email=models.EmailField()
	def __str__(self):
		return self.name



class Subscriber(models.Model):
	email=models.EmailField()
	date=models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return self.email
